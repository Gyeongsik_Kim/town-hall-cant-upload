package com.gyungdal.tab;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.gyungdal.tab.audio.Play;
import com.gyungdal.tab.clock.Clock12;
import com.gyungdal.tab.clock.Clock24;
import com.gyungdal.tab.service.UsbService;

import java.io.File;
import java.lang.Override;
import java.lang.ref.WeakReference;
import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.Locale;
import java.util.Set;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getName();
    private static final String NOT_HUMAN = "H";
    private static final String FIND_PEOPLE = "0";
    private static final String FIRE = "F";
    private static final String FIRE_DONE = "f";
    private static final String HUMAN = "h";
    public static UsbService usbService;
    private MyHandler mHandler;
    private static Clock12 clock12;
    private static Clock24 clock24;
    private static TextView outText;
    private static Play play;
    private static Calendar startday;
    public static boolean isOut, isCall, isPlay, isRecord, inPeople, time12, isFire;
    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case UsbService.ACTION_USB_PERMISSION_GRANTED: // USB PERMISSION GRANTED
                    Toast.makeText(context, "USB Ready", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_USB_PERMISSION_NOT_GRANTED: // USB PERMISSION NOT GRANTED
                    Toast.makeText(context, "USB Permission not granted", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_NO_USB: // NO USB CONNECTED
                    Toast.makeText(context, "No USB connected", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_USB_DISCONNECTED: // USB DISCONNECTED
                    Toast.makeText(context, "USB disconnected", Toast.LENGTH_SHORT).show();
                    break;
                case UsbService.ACTION_USB_NOT_SUPPORTED: // USB NOT SUPPORTED
                    Toast.makeText(context, "USB device not supported", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private final ServiceConnection usbConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName arg0, IBinder arg1) {
            usbService = ((UsbService.UsbBinder) arg1).getService();
            usbService.setHandler(mHandler);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            usbService = null;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLocale("en");
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init(){
        mHandler = new MyHandler(this);
        clock12 = (Clock12) findViewById(R.id.clock12);
        clock24 = (Clock24) findViewById(R.id.clock24);
        outText = (TextView) findViewById(R.id.outText);
        Typeface cfont = Typeface.createFromAsset(getAssets(), "fonts/DS_DIGIB.TTF");
        clock12.setTypeface(cfont);
        clock24.setTypeface(cfont);
        clock24.setVisibility(View.VISIBLE);
        outText.setVisibility(View.INVISIBLE);
        clock12.setVisibility(View.INVISIBLE);
        isOut = time12 = isCall = false;
        play = new Play(getApplicationContext());
    }

    private void toast(String msg){
        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_SHORT).show();
    }

    public void Click(View v){
        switch(v.getId()){
            case R.id.out :
                if(!isPlay && !isRecord) {
                    if(!isOut) {
                        isOut = true;
                        play.raw("out");
                        outText.setText("외출중");
                        outText.setVisibility(View.VISIBLE);
                        clock24.setVisibility(View.INVISIBLE);
                        clock12.setVisibility(View.INVISIBLE);
                        usbService.write("o".getBytes());
                    }else{
                        usbService.write("O".getBytes());
                        isOut = false;
                        outText.setVisibility(View.INVISIBLE);
                        if(time12) {
                            clock24.setVisibility(View.INVISIBLE);
                            clock12.setVisibility(View.VISIBLE);
                        }else{
                            clock24.setVisibility(View.VISIBLE);
                            clock12.setVisibility(View.INVISIBLE);
                        }
                    }
                }
                Log.i(TAG, "외출");
                break;
            case R.id.setClock :
                outText.setVisibility(View.INVISIBLE);
                if(clock24.getVisibility() == View.VISIBLE){
                    clock24.setVisibility(View.INVISIBLE);
                    clock12.setVisibility(View.VISIBLE);
                    time12 = false;
                }else{
                    clock24.setVisibility(View.VISIBLE);
                    clock12.setVisibility(View.INVISIBLE);
                    time12 = true;
                }
                break;
            case R.id.safeCall :
                if(isRecord)
                    toast("방송을 녹음중입니다.");
                else{
                    if(!isPlay) {
                        play.loopRaw("sound");
                        outText.setText("호출중");
                        outText.setVisibility(View.VISIBLE);
                        clock24.setVisibility(View.INVISIBLE);
                        clock12.setVisibility(View.INVISIBLE);
                        Log.i(TAG, "안심콜");
                        usbService.write("s".getBytes());
                    }else{
                        play.sound.stop();
                        play.sound.release();
                        isPlay = false;
                        usbService.write("S".getBytes());
                        outText.setVisibility(View.INVISIBLE);
                        if(time12) {
                            clock24.setVisibility(View.INVISIBLE);
                            clock12.setVisibility(View.VISIBLE);
                        }else{
                            clock24.setVisibility(View.VISIBLE);
                            clock12.setVisibility(View.INVISIBLE);
                        }
                    }
                }

            break;
            default:
                toast("????");

        }
    }

    private void setLocale(String Charicter) {
        Locale locale = new Locale(Charicter);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
    }


    @TargetApi(19)
    private void startService(Class<?> service, ServiceConnection serviceConnection, Bundle extras) {
        if (!UsbService.SERVICE_CONNECTED) {
            Intent startService = new Intent(this, service);
            if (extras != null && !extras.isEmpty()) {
                Set<String> keys = extras.keySet();
                for (String key : keys) {
                    String extra = extras.getString(key);
                    startService.putExtra(key, extra);
                }
            }
            startService(startService);
        }
        Intent bindingIntent = new Intent(this, service);
        bindService(bindingIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onResume() {
        super.onResume();
        setFilters();  // Start listening notifications from UsbService.
        startService(UsbService.class, usbConnection, null); // Start UsbService(if it was not started before) and Bind it
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Toast.makeText(getApplicationContext(), "막힌 기능 입니다.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPause() {
        super.onPause();
        unregisterReceiver(mUsbReceiver);
        unbindService(usbConnection);
    }

    private void setFilters() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UsbService.ACTION_USB_PERMISSION_GRANTED);
        filter.addAction(UsbService.ACTION_NO_USB);
        filter.addAction(UsbService.ACTION_USB_DISCONNECTED);
        filter.addAction(UsbService.ACTION_USB_NOT_SUPPORTED);
        filter.addAction(UsbService.ACTION_USB_PERMISSION_NOT_GRANTED);
        registerReceiver(mUsbReceiver, filter);
    }

    /*
     * This handler will be passed to UsbService. Data received from serial port is displayed through this handler
     */
    private static class MyHandler extends Handler {
        private final WeakReference<MainActivity> mActivity;

        public MyHandler(MainActivity activity) {
            mActivity = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case UsbService.MESSAGE_FROM_SERIAL_PORT:
                    if(msg.obj.equals(FIND_PEOPLE))
                        inPeople = true;
                    else if(msg.obj.equals(FIRE)){
                        outText.setText("화재 감지");
                        isFire = true;
                        outText.setVisibility(View.VISIBLE);
                        clock24.setVisibility(View.INVISIBLE);
                        clock12.setVisibility(View.INVISIBLE);
                        if(!isPlay)
                            play.radio("fireWarning.wav");

                    }else if(msg.obj.equals(FIRE_DONE) && isFire){
                        outText.setVisibility(View.INVISIBLE);
                        isFire = false;
                        if(time12) {
                            clock24.setVisibility(View.INVISIBLE);
                            clock12.setVisibility(View.VISIBLE);
                        }else{
                            clock24.setVisibility(View.VISIBLE);
                            clock12.setVisibility(View.INVISIBLE);
                       }
                    }else if(msg.obj.equals(HUMAN)){
                            if(!isPlay)
                                play.raw("security");
                        if(isOut){
                            outText.setText("방범");
                            outText.setVisibility(View.VISIBLE);
                            clock24.setVisibility(View.INVISIBLE);
                            clock12.setVisibility(View.INVISIBLE);
                        }
                        startday = Calendar.getInstance();
                    }else if(msg.obj.equals(NOT_HUMAN)) {
                        if (startday != null) {
                            Calendar today = Calendar.getInstance();
                            long gapDay = ((today.getTimeInMillis() - startday.getTimeInMillis()) / 1000) / (60 * 60 * 24);
                            if (gapDay >= 1)
                                if(!isPlay)
                                   play.raw("alonedie");
                        }
                        if(isOut){
                            outText.setText("외출");
                            outText.setVisibility(View.VISIBLE);
                            clock24.setVisibility(View.INVISIBLE);
                            clock12.setVisibility(View.INVISIBLE);
                        }
                    }

                    Log.i("Serial Data", (String) msg.obj);

                    break;
            }
        }
    }
}
package com.gyungdal.tab.audio;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by GyungDal on 2016-05-10.
 */
public class Download extends AsyncTask<String, Integer, Boolean>{
    private static final String Path = Environment.DIRECTORY_MUSIC + File.separator + "SOS" + File.separator + "sound.mp3";
    private static final String TAG = Download.class.getName();
    @Override
    protected Boolean doInBackground(String... params) {
        File file = new File(Path);
        if(file.exists())
            file.delete();

        try{
            URL url = new URL(params[0]);
            URLConnection conn = url.openConnection();
            int lenght = conn.getContentLength();
            DataInputStream stream = new DataInputStream(url.openStream());
            byte[] buffer = new byte[lenght];
            stream.readFully(buffer);
            stream.close();
            DataOutputStream fos = new DataOutputStream(new FileOutputStream(file));
            fos.write(buffer);
            fos.flush();
            fos.close();
        }catch (MalformedURLException e){
            Log.e(TAG, e.getMessage());
        }catch(IOException e){
            Log.e(TAG, e.getMessage());
        }
        return true;
    }
}

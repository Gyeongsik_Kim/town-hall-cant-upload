package com.gyungdal.tab.audio;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.gyungdal.tab.MainActivity;
import java.io.File;

/**
 * Created by GyungDal on 2016-05-06.
 */
public class Play {
    private AudioManager am;
    public MediaPlayer sound;
    private Context context;

    public Play(Context context){
        this.context = context;
    }
    private boolean getAudioFocus(){
        am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        am.setStreamVolume(AudioManager.STREAM_MUSIC,
                am.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                AudioManager.FLAG_PLAY_SOUND);
        int result = am.requestAudioFocus(null, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        if (result != AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            Log.e(Play.class.getName(), "Get Audio Focus Fail");
            return false;
        }
        return true;
    }

    public boolean raw(String fname){
        try {
            if(!getAudioFocus())
                return false;
            sound = new MediaPlayer();
            AssetFileDescriptor music = context.getResources().
                    openRawResourceFd(context.getResources().getIdentifier(fname , "raw" , context.getPackageName()));
            sound.setDataSource(music.getFileDescriptor(), music.getStartOffset(), music.getLength());
            sound.prepare();
            sound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            sound.stop();
                    sound.release();
                    MainActivity.usbService.write("S".getBytes());
                    MainActivity.isPlay = false;
                }
            });
            MainActivity.isPlay = true;
            sound.start();
            music.close();
        }catch(Exception e){
            error(e.getMessage());
        }
        return true;
    }

    public boolean radio(String fname){
         try{
             if(!getAudioFocus())
                return false;
             sound = new MediaPlayer();
             String audioPath = Environment.getExternalStorageDirectory().getAbsolutePath()+ File.separator + fname;
             Log.i("Audio", audioPath);
             //FileInputStream fis = new FileInputStream(audioPath);
             //FileDescriptor fd = fis.getFD();
             sound.setDataSource(audioPath);
             sound.prepare();
             sound.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                 @Override
                 public void onCompletion(MediaPlayer mp) {
                     sound.stop();
                     sound.release();
                     MainActivity.usbService.write("S".getBytes());
                     MainActivity.isPlay = false;
                 }
             });
             MainActivity.isPlay = true;
             sound.start();
         }catch(Exception e){
             error(e.getMessage());
             Toast.makeText(context, "저장된 방송이 없습니다.", Toast.LENGTH_SHORT).show();
         }
        return true;
    }


    public boolean loopRaw(String fname){
        try {
            if(!getAudioFocus())
                return false;
            sound = new MediaPlayer();
            AssetFileDescriptor music = context.getResources().
                    openRawResourceFd(context.getResources().
                            getIdentifier(fname , "raw" , context.getPackageName()));
            sound.setDataSource(music.getFileDescriptor(), music.getStartOffset(), music.getLength());
            sound.prepare();
            sound.setLooping(true);
            MainActivity.isPlay = true;
            sound.start();
            music.close();

        }catch(Exception e){
            error(e.getMessage());
        }
        return true;
    }

    private void error(String msg){
        Log.e(getClass().getName(), msg);
    }
}

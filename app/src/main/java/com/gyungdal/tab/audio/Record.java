package com.gyungdal.tab.audio;

import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.gyungdal.tab.MainActivity;

import java.io.File;

/**
 * Created by GyungDal on 2016-05-09.
 */
// TODO : 이거 되는지 잘 모름
public class Record extends AsyncTask<Void,Void,Void>{
    private MediaRecorder recorder;
    private static final double REFERENCE = 0.1;
    private static final String TAG = Record.class.getName();

    public Record(){
        recorder = new MediaRecorder();
    }

    public void stop(){
        MainActivity.isRecord = false;
        recorder.stop();
        recorder.release();
    }

    @Override
    protected Void doInBackground(Void... params) {
        File file = new File(Environment.DIRECTORY_MUSIC + File.separator + "SOS" + File.separator);
        if(!file.exists())
            file.mkdirs();

        while(true) {
            file = new File(Environment.DIRECTORY_MUSIC + File.separator + "SOS" + File.separator + "sound.mp3");
            if(file.exists())
                file.delete();
            MainActivity.isRecord = true;
            recorder.setOutputFile(Environment.DIRECTORY_MUSIC + File.separator + "SOS" + File.separator + "sound.mp3");
            recorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
            recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

            try {
                recorder.prepare();
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
            recorder.start();
            stop();
        }
    }
}
